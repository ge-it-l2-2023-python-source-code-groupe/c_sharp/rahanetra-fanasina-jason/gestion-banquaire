﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using Microsoft.VisualBasic;
using ClassClient;
using ClassCompte;

namespace Banque
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice="yes";
            int i=0;

            while (choice =="yes")
            {
                Console.WriteLine("voulez-vous créer votre compte (yes or no) : ");
                choice = Console.ReadLine()?? "";
                i ++;
                Console.WriteLine($"Compte {i}");
                if (choice == "yes")
                {
                    Console.WriteLine("Veuillez remplir les détails suivants :\n");

                    Console.Write("Entrer votre cin: ");
                    string cin = Console.ReadLine()?? "";

                    Console.Write("Entrer votre nom : ");
                    string nom = Console.ReadLine()?? "";

                    Console.Write("Entrer votre prénom : ");
                    string prenom = Console.ReadLine()?? "";

                    Console.Write("Entrer votre numéros de téléphone : ");
                    string num = Console.ReadLine()?? "";

                    Console.WriteLine("************************************");

                    Client client1 = new Client (cin,nom,prenom,num);
                    Compte compte1 = new Compte (client1);
                    compte1.Afficher();

                    Console.WriteLine("************************************");
                    Console.WriteLine("Donner le montant à déposer : ");
                    int somme= 0;
                    string input = Console.ReadLine()?? "";
                    try{
                    somme = int.Parse(input);
                    }
                    catch(FormatException){
                        Console.WriteLine("Erreur de format.");
                    }
                    Console.WriteLine("Opération bien effectuée");
                    Console.WriteLine("************************************");
                    compte1.Crediter(somme);
                    compte1.Afficher();
                    Console.WriteLine("Donner le montant à rétirer : ");
                    string input1 = Console.ReadLine()?? "";
                    try{
                      somme = int.Parse(input1);
                    }
                    catch(FormatException){
                        Console.WriteLine("Erreur de format.");
                    }
                    Console.WriteLine("Opération bien effectuée");
                    Console.WriteLine("************************************");
                    compte1.Debiter(somme);
                    compte1.Afficher();
                }
                else 
                {
                    break;
                }
            }
            Console.WriteLine($"Le nombre de comptes créés : {i-1}");
        }
    }

}

