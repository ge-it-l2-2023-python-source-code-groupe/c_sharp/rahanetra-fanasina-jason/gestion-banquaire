namespace ClassClient;
class Client
    {
        public string Cin {get; private set;}
        public string Nom {get; private set;}
        public string Prenom {get; private set;}
        public string Tel {get; private set;}


        public Client(string cinClient, string nomClient, string prenomClient, string telClient)
        {
            Cin = cinClient;
            Nom = nomClient;
            Prenom = prenomClient;
            Tel = telClient;
        }
        public Client(string cinClient, string nomClient, string prenomClient)
        {
            Cin = cinClient;
            Nom = nomClient;
            Prenom = prenomClient;
            Tel = "";
        }
        public void Afficher()
        {
            
            Console.WriteLine($@"
            CIN: {Cin}
            NOM: {Nom}
            Prénom: {Prenom}
            Tél: {Tel}
            ************************");
        }
    }