using ClassClient;

namespace ClassCompte
{
    class Compte
    {
        private long solde;
        public long Solde{get { return solde; } }
        public uint Code{get;}    
        public Client Clientelle{get; set;}
        private uint Num = 1;
    
        public Compte(Client cli)
        {
            solde = 0;
            Code = Num;
            Num ++;
            Clientelle = cli;
        }
        public void Crediter(int somme)
        {
            solde +=somme;
        }
        public void Crediter(int somme, Compte debiteur)
        {
            Crediter(somme);
            debiteur.Debiter(somme);
        }
        public void Debiter(int somme)
        {
            solde -=somme;
        }
        public void Debiter(int somme, Compte crediteur)
        {
            Debiter(somme);
            crediteur.Crediter(somme);
        }
        public void Afficher()
        {
            Console.WriteLine($@"
            Numéro de Compte: {Code}
            Solde de compte: {Solde}
            Propriétaire du compte :
            CIN: {Clientelle.Cin}
            NOM: {Clientelle.Nom}
            Prénom: {Clientelle.Prenom}
            Tél : {Clientelle.Tel}");
        }
    }
}
